pbbarcode (0.8.0-6) UNRELEASED; urgency=medium

  * Use 2to3 to port to Python3
    Closes: #937253
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g
  * Set upstream metadata fields: Repository, Repository-Browse.
  TODO: pbh5tools (#937256)

 -- Andreas Tille <tille@debian.org>  Sat, 30 Nov 2019 17:59:51 +0100

pbbarcode (0.8.0-5) unstable; urgency=medium

  * Team upload.
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Secure URI in copyright format
  * Respect DEB_BUILD_OPTIONS in override_dh_auto_test target
  * Remove trailing whitespace in debian/copyright

 -- Andreas Tille <tille@debian.org>  Wed, 31 Oct 2018 08:07:56 +0100

pbbarcode (0.8.0-4) unstable; urgency=medium

  * Team upload
  * Upstream closed issue #3 and added tag - watch file written
  * Add README.Debian with note from upstream that this is a
    deprecated repo
  * Readme.test: Add note about test that take *very* long and are
    deactivated.

 -- Andreas Tille <tille@debian.org>  Tue, 17 Jan 2017 08:24:50 +0100

pbbarcode (0.8.0-3) unstable; urgency=medium

  * Team upload.
  * Reorganise autopkgtest to make it a user example at the same time
  * remove some test that takes incredibly long time to end up in a 'timed out'
    failure in autopkgtest
  * changes of `cme fix dpkg-control` without reformatting
  * debhelper 10
  * Add fake watch file
  * debian/tests/control:
     - Make lintian happy with formatting
     - Drop dependency make which is not needed any more
  * Fix spelling errors in doc
  * hardening=+all

 -- Andreas Tille <tille@debian.org>  Sun, 15 Jan 2017 12:44:42 +0100

pbbarcode (0.8.0-2) unstable; urgency=low

  * Update build/test-dependency for pbh5tools
  * Update Standards-Version to 3.9.7
  * Use encrypted protocols for VCS URLs

 -- Afif Elghraoui <afif@ghraoui.name>  Sat, 20 Feb 2016 01:42:05 -0800

pbbarcode (0.8.0-1) unstable; urgency=low

  * Initial release (Closes: #808202)

 -- Afif Elghraoui <afif@ghraoui.name>  Fri, 18 Dec 2015 22:38:07 -0800
